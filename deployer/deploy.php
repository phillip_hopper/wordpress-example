<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 9/30/16
 * Time: 4:32 PM
 *
 * Borrowed from: vendor/deployer/deployer/recipe/symfony.php
 */

use function Deployer\after;
use function Deployer\desc;
use function Deployer\get;
use function Deployer\has;
use function Deployer\input;
use function Deployer\inventory;
use function Deployer\run;
use function Deployer\set;
use function Deployer\task;

require_once '../vendor/autoload.php';
require_once 'recipe/common.php';

set('default_stage', 'prod');
set('repository', 'git@bitbucket.org:rvwholesalers/indian-hallow.git');
set('shared_dirs', ['config', '.well-known']);
set('writable_dirs', ['.well-known']);

inventory('servers.yml');

task('deploy:wp_config_symlink', function () {

    // create link to the shared wp_config.php file
    run("ln -sf {{deploy_path}}/shared/wp-config.php {{release_path}}/wordpress/wp-config.php");

})->desc('Creating symlink to wordpress config file.');

desc('Update code');
task('deploy:update_code_ex', function () {
    $repository = trim(get('repository'));
    $branch = get('branch');
    $git = get('bin/git');
    $gitCache = get('git_cache');
    $depth = $gitCache ? '' : '--depth 1';
    $options = [
        'tty' => get('git_tty', false),
    ];

    // If option `branch` is set.
    if (input()->hasOption('branch')) {
        $inputBranch = input()->getOption('branch');
        if (!empty($inputBranch)) {
            $branch = $inputBranch;
        }
    }

    // Branch may come from option or from configuration.
    $at = '';
    if (!empty($branch)) {
        $at = "-b $branch";
    }

    // If option `tag` is set
    if (input()->hasOption('tag')) {
        $tag = input()->getOption('tag');
        if (!empty($tag)) {
            $at = "-b $tag";
        }
    }

    // If option `tag` is not set and option `revision` is set
    if (empty($tag) && input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            $depth = '';
        }
    }

    if ($gitCache && has('previous_release')) {
        try {
            // run("$git clone $at --recursive -q --reference {{previous_release}} --dissociate $repository  {{release_path}} 2>&1", $options);
            run("ssh-agent sh -c 'ssh-add ~/.ssh/id_rsa; $git clone $at --reference {{previous_release}} --dissociate $repository  {{release_path}} 2>&1'", $options);
        } catch (RuntimeException $exc) {
            // If {{deploy_path}}/releases/{$releases[1]} has a failed git clone, is empty, shallow etc, git would throw error and give up. So we're forcing it to act without reference in this situation
            //run("$git clone $at --recursive -q $repository {{release_path}} 2>&1", $options);
            run("ssh-agent sh -c 'ssh-add ~/.ssh/id_rsa; $git clone $at $repository {{release_path}} 2>&1'", $options);
        }
    } else {
        // if we're using git cache this would be identical to above code in catch - full clone. If not, it would create shallow clone.
        //run("$git clone $at $depth --recursive -q $repository {{release_path}} 2>&1", $options);
        run("ssh-agent sh -c 'ssh-add ~/.ssh/id_rsa; $git clone $at $depth $repository {{release_path}} 2>&1'", $options);
    }

    if (!empty($revision)) {
        run("cd {{release_path}} && $git checkout $revision");
    }
});

/**
 * Main task
 */
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code_ex',
    'deploy:shared',
    'deploy:symlink',
    'deploy:wp_config_symlink',
    'cleanup',
])->desc('Deploy your project');

after('deploy', 'success');
