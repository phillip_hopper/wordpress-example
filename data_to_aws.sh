#!/usr/bin/env bash

dumpDir="/tmp/indian_hallow/"
dumpFile="${dumpDir}to_aws.sql"
localDb="indian_hallow"
remoteDb="indian_hallow"

echo "Renaming old dump files"

if [ -f ${dumpFile} ]
then
	mv --backup=numbered "${dumpFile}" "${dumpFile}.bak"
fi

echo "Creating ${dumpDir}"

mkdir -p "${dumpDir}"

echo "Backing up ${localDb}"

mysqldump --defaults-file=config/my.local.conf --default-character-set=utf8 --add-drop-database --routines --result-file="${dumpFile}" --databases "${localDb}"

echo "Restoring to ${remoteDb}"

mysql --defaults-file=config/my.aws.conf --default-character-set=utf8 --database=${remoteDb} < "${dumpFile}"

echo "Fixing host name"

php srdb/srdb.cli.php --defaults="../config/my.aws.conf" -n ${remoteDb} -s "http://hallow.localhost" -r "https://indianhallowcampground.com"

echo "Finished"
