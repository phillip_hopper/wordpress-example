#!/usr/bin/env bash

dumpDir="/tmp/indian_hallow/"
dumpFile="${dumpDir}from_aws.sql"
localDb="indian_hallow"
remoteDb="indian_hallow"

echo "Renaming old dump files"

if [ -f ${dumpFile} ]
then
	mv --backup=numbered "${dumpFile}" "${dumpFile}.bak"
fi

echo "Creating ${dumpDir}"

mkdir -p "${dumpDir}"

echo "Backing up ${remoteDb}"

mysqldump --defaults-file=config/my.aws.conf --default-character-set=utf8 --add-drop-database --routines --result-file="${dumpFile}" --databases "${remoteDb}"

echo "Restoring to ${localDb}"

mysql --defaults-file=config/my.local.conf --default-character-set=utf8 < "${dumpFile}"

echo "Fixing host name"

php srdb/srdb.cli.php --defaults="../config/my.local.conf" -n ${localDb} -s "http://indianhallowcampground.com" -r "http://hallow.localhost"
php srdb/srdb.cli.php --defaults="../config/my.local.conf" -n ${localDb} -s "http://www.indianhallowcampground.com" -r "http://hallow.localhost"
php srdb/srdb.cli.php --defaults="../config/my.local.conf" -n ${localDb} -s "https://indianhallowcampground.com" -r "http://hallow.localhost"
php srdb/srdb.cli.php --defaults="../config/my.local.conf" -n ${localDb} -s "https://www.indianhallowcampground.com" -r "http://hallow.localhost"

echo "Finished"
